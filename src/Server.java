import java.net.*;

public class Server {
	public static void main(String args[]) throws Exception {

		int porta = 9876;
		int numConn = 1;
		
		@SuppressWarnings("resource")
		DatagramSocket serverSocket = new DatagramSocket(porta);

		byte[] DadosReceber = new byte[100];
		byte[] DadosEnviar = new byte[256];

		while (true) {

			DatagramPacket pacoteReceber = new DatagramPacket(DadosReceber,
					DadosReceber.length);
			System.out.println("Esperando por dados na porta " + porta);
			serverSocket.receive(pacoteReceber);
			System.out.print("\nDados [" + numConn + "] recebido...");

			String stringRecebida = new String(pacoteReceber.getData());
			System.out.println("\nString Recebida: " + stringRecebida);
			System.out.println("\nDe " + pacoteReceber.getAddress() + "\nTamanho: " + pacoteReceber.getLength());
			
			InetAddress IPAddress = pacoteReceber.getAddress();

			int port = pacoteReceber.getPort();
			// converte em maisculo a string recebida			
			String stringMaiusc = stringRecebida.toUpperCase();
			
			//string reversa
			String invertida = new StringBuilder(stringRecebida).reverse().toString();
			
			// conta quantos caracteres a string possui
			int stringTamanho =  pacoteReceber.getLength();
			
			String pacoteEnviados ="Maiscula: " + stringMaiusc + "\nTamanho: "+Integer.toString(stringTamanho) + "\nInvertida: " + invertida;
			
			DadosEnviar = pacoteEnviados.getBytes();
			//DadosEnviar = stringMaiusc.getBytes();
						
			DatagramPacket pacoteEnviar = new DatagramPacket(DadosEnviar,
			DadosEnviar.length, IPAddress, port);
			
			System.out.print("Enviando " + stringMaiusc + " Tamanho "+ stringTamanho);

			serverSocket.send(pacoteEnviar);
			System.out.println("\nPacote enviado!\n");
			System.out.println(pacoteEnviados);
			System.out.println("\nTamanho do pacote: "+pacoteEnviados.length());
			System.out.println("\nenviado o pacote");
			
			numConn++;
		

		}
	}
}